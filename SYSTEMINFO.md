# Cisco ISE

Vendor: Cisco
Homepage: https://www.cisco.com/

Product: Cisco Identity Services Engine (ISE)
Product Page: https://www.cisco.com/site/us/en/products/security/identity-services-engine/index.html

## Introduction
We classify Cisco ISE into the Security/SASE domain as Cisco ISE provides the solution to enhance network security and manage network access.

"Cisco ISE gives you visibility and control over who and what is on the network."

## Why Integrate
The Cisco ISE adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco ISE. With this adapter you have the ability to perform operations such as:

- Automates the building and management of Cisco identity services engine resources.
- Create Network Devices
- Get Network Devices

## Additional Product Documentation
The [API documents for Cisco ISE](https://developer.cisco.com/docs/identity-services-engine/latest/)