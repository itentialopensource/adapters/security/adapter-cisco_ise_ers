
## 0.7.4 [10-15-2024]

* Changes made at 2024.10.14_20:42PM

See merge request itentialopensource/adapters/adapter-cisco_ise_ers!20

---

## 0.7.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cisco_ise_ers!18

---

## 0.7.2 [08-14-2024]

* Changes made at 2024.08.14_18:59PM

See merge request itentialopensource/adapters/adapter-cisco_ise_ers!17

---

## 0.7.1 [08-07-2024]

* Changes made at 2024.08.06_20:11PM

See merge request itentialopensource/adapters/adapter-cisco_ise_ers!16

---

## 0.7.0 [05-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!15

---

## 0.6.10 [03-27-2024]

* Changes made at 2024.03.27_13:46PM

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!14

---

## 0.6.9 [03-13-2024]

* Add task with query input

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!13

---

## 0.6.8 [03-13-2024]

* Changes made at 2024.03.13_11:09AM

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!12

---

## 0.6.7 [03-11-2024]

* Changes made at 2024.03.11_16:17PM

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!10

---

## 0.6.6 [02-27-2024]

* Changes made at 2024.02.27_11:52AM

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!9

---

## 0.6.5 [02-21-2024]

* broker mappings

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!8

---

## 0.6.4 [02-08-2024]

* fix filter

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!7

---

## 0.6.3 [01-09-2024]

* fix filter

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!7

---

## 0.6.2 [01-09-2024]

* fix filter on searchNetworkDevice

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!6

---

## 0.6.1 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!5

---

## 0.6.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!4

---

## 0.5.0 [11-06-2023]

* More migration changes

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!4

---

## 0.4.0 [10-19-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!3

---

## 0.3.0 [09-20-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!3

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!2

---

## 0.1.1 [03-03-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-cisco_ise_ers!1

---
